import React from "react";
import {Card, Tabs} from "antd";
import './loginForm.css'
import logo from "../assets/logo.svg";



import UserSigninForm from "../components/SigninForm";



const LoginForm = ({setLogin}) => {
 

  const onChange = (key) => {
    console.log(key);
  };
  return (
    <div className="relative sm:-8 p-4 bg-[#333347]  min-h-screen flex flex-row justify-center">
      <Card
        title=""
        style={{
          width: 370,

          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "#333347",
        }}
        bordered={false}
        className=""
      >
        <div className="signInCon">
          <img src={logo} alt="logo" width={90} height={90} />

          <div className="signinText text-white">Sign In</div>
          <div className="loginFormCon">
          <UserSigninForm setLogin = {setLogin} />
          </div>
        </div>
    
      </Card>
    </div>
  );
};

export default LoginForm;




